import java.io.IOException;
import java.util.*;

final class Lexer {

    static List<Token> parse(String string) throws IOException {

        boolean waitingForSuckass = true;

        List<Token> tokens = new LinkedList<>();
        StringBuilder accumulator = new StringBuilder();

        for (int i = 0; i < string.length(); i++) {

            if (waitingForSuckass) {

                if (accumulator.toString().equals(""))
                    accumulator.append(string.charAt(i));

                if (search(accumulator.toString()))
                    waitingForSuckass = false;

                if (waitingForSuckass)
                    throw new IOException("Unexpected symbol \'" + string.charAt(i) + "\' " + " position: " + i);

            } else {

                accumulator.append(string.charAt(i));

                if (!search(accumulator.toString())) {
                    accumulator.deleteCharAt(accumulator.length() - 1);
                    tokens.add(new Token(accumulator.toString()));
                    accumulator = new StringBuilder();
                    waitingForSuckass = true;
                    i--;
                }
            }
        }

        if (!tokens.isEmpty())
            tokens.add(new Token(accumulator.toString()));

        return tokens;
    }

    private static boolean search(String string) {
        for (Lexeme lexeme : Lexeme.values())
            if (string.matches(lexeme.getPattern().toString()))
                return true;

        return false;
    }
}
