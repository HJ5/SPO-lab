public interface DumbCollection {

    public void add(Object object);
    public void remove(Object object);
    public void clear();
}
