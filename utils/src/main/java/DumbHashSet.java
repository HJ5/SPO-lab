public class DumbHashSet implements DumbCollection {

    private Entry[] buckets;
    private int size;
    private int capacity = 20;

    private static class Entry {
        Object key;
        Entry next;
    }

    DumbHashSet() {
        buckets = new Entry[capacity];
        size = 0;
    }

    public int size() {
        return size;
    }

    public DumbHashSet(int capacity) {
        this.capacity = capacity;
        buckets = new Entry[capacity];
        size = 0;
    }

    private int hashFunction(int hashCode) {
        int index = hashCode;

        if (index < 0)
            index = -index;

        return index % buckets.length;
    }

    public boolean contains(Object element) {
        int index = hashFunction(element.hashCode());
        Entry current = buckets[index];

        while (current != null) {
            if (current.key.equals(element))
                return true;

            current = current.next;
        }

        return false;
    }

    public void add(Object element) {
        int index = hashFunction(element.hashCode());

        Entry current = buckets[index];

        while (current != null) {
            if (current.key.equals(element))
                return;

            current = current.next;
        }

        Entry entry = new Entry();
        entry.key = element;

        entry.next = buckets[index];
        buckets[index] = entry;
        size++;

    }

    public void remove(Object element) {
        int index = hashFunction(element.hashCode());

        Entry current = buckets[index];
        Entry previous = null;

        while (current != null) {

            if (current.key.equals(element)) {

                if (previous == null)
                    buckets[index] = current.next;
                else
                    previous.next = current.next;

                size--;
                return;
            }

            previous = current;
            current = current.next;
        }
    }

    public void clear() {
        buckets = new Entry[capacity];
        size = 0;
    }

    @Override
    public String toString() {
        Entry currentEntry = null;
        StringBuilder sb = new StringBuilder();

        if (this.size > 0) {
            sb.append("[");

            for (int i = 0; i < buckets.length; i++) {
                if (buckets[i] != null) {
                    currentEntry = buckets[i];
                    sb.append("[").append(i).append(" : ");
                    sb.append(currentEntry.key.toString()).append("], ");
                }
            }

            sb.delete(sb.length() - 2, sb.length());
            sb.append("]");

            return sb.toString();

        } else {
            return "[]";
        }
    }
}